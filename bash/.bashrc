stty -ixon #disable lock console
#set ibean cursor
##printf '\e[6 q'

# -- BUILDIN CONFIGS --
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# vi mode
set -o vi
# FIX Ctrl+l in vi mode
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# -- UMASK --
umask 0077
#umask 0022 #root

#-- PS1 --
PS1='$(ecod=$? ; [ $PWD != $HOME ] && printf "\w " ; [ -n "$(jobs -p)" ] && printf \[\e[93m\]jobs\ \j ; __git_ps1 "\[\e[35m\]{ %s }" ; [ $ecod != 0 ] && printf \[\e[33m\] || printf \[\e[32m\])>>\[\e[0m\] '

# -- PATH --
# for debian
#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin #root path
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$HOME/.local/bin"

# -- HISTORY --

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000
# append to the history file, don't overwrite it
shopt -u histappend

# -- ENV --
export EDITOR=nvim

# -- Functions --

complete -cf root
function root(){
	if [ "$#" -eq 0 ];then
		su
	else
		comand="${@:1}"
		su -c "$comand"
	fi
}

# Set 'man' colors
function man(){
	env \
	LESS_TERMCAP_mb=$'\e[01;32m' \
	LESS_TERMCAP_md=$'\e[01;36m' \
	LESS_TERMCAP_me=$'\e[0m' \
	LESS_TERMCAP_se=$'\e[0m' \
	LESS_TERMCAP_so=$'\e[01;44;37m' \
	LESS_TERMCAP_ue=$'\e[0m' \
	LESS_TERMCAP_us=$'\e[01;32m' \
	man "$@"
}


function rmk(){
	scrub -p dod $1
	shred -zun 10 -v $1
}

#function apt(){
#case $1 in
#	upgrade|dist-upgrade)
#                supgrade
#        ;;
#        full-upgrade)
#                if [ "$2" == '--force' ];then
#                        /usr/bin/apt full-upgrade
#                else
#                        supgrade
#                fi
#        ;;
#        *)
#                /usr/bin/apt $@
#        ;;
#esac
#}


#function systemctl(){
#if [ "$1" == "poweroff" ] || [ "$1" == "reboot" ];then
#                if [ $(tty) == '/dev/tty1' ];then
#			printf '%s in 6 seconds' $1
#			sleep 6
#                	/usr/bin/systemctl $1
#                else
#                        printf '\e[31m not permit\n tty is not tty1 !!!\n\e[0m'
#                fi
#else
#        	/usr/bin/systemctl $@
#fi
#}

function get(){
	curl --progress-bar -O -L $1
}

function tempdir(){
	temp_directory=/tmp/$USER.*
	if [ ! -d $temp_directory ];then
		temp_directory=$(mktemp -d /tmp/$USER.XXX)
	fi
	cd $temp_directory
}

#function hex-encode(){
#	echo "$@" | xxd -p
#}
#
#function hex-decode(){
#	echo "$@" | xxd -p -r
#    }

#function rot13(){
#	echo "$@" | tr 'A-Za-z' 'N-ZA-Mn-za-m'
#}

## -- Alias --
#alias apt-get='apt'
alias ..='cd ..'
alias bat='batcat'
alias cat='batcat -P'
alias cll='clear ; ls'
alias cls='clear'
alias cp='cp -v'
alias cdo='cd $OLDPWD'
alias dd='dd status=progress'
alias dir='dir --color=auto'
alias disable-his='set +o history'
alias du='du -h'
alias enable-his='set -o history'
alias grep='grep --color=auto'
alias ip='ip -c=auto'
alias ipa='ip -c=auto a'
alias lj='jobs'
alias ls='exa'
alias ll='exa -la --octal-permissions'
alias mv='mv -v'
alias myip='curl ipinfo.io/ip;echo'
alias n='nano'
alias net='ping -c 1 8.8.8.8 &>/dev/null && printf "\e[32mCon internet\n\e[0m" || printf "\e[31mSin internet\n\e[0m"'
alias pedco='ping -c 1 pedco.uncoma.edu.ar &>/dev/null && printf "\e[32mPedco funca \n\e[0m" || printf "\e[31mSe cayo la weah \n\e[0m"'
alias nino='arduino-cli sketch new'
alias rm='rm -i'
alias duplicated='find -not -empty -type f -printf "%s\n" | sort -rn | uniq -d | xargs -I{} -n1 find -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate'
alias ssid='iw dev wlp2s0 info | grep "ssid" |cut -d " " -f2'
#alias su='su -P -l'
alias poweroff='systemctl poweroff'
alias q='exit'
alias !q='exit'
alias reboot='systemctl reboot'
alias tree='exa -aT --color=always --group-directories-first'
alias v='nvim'
alias vim='nvim'
alias copyx='xclip -sel clip'
alias ydl='youtube-dl'
alias yta="youtube-dl -o '%(title)s' -f 140"


# -- GIT --
alias g='git'
alias gadd='git add'
alias gchk='git checkout'
alias gcl='git clone'
alias gcl1='git clone --depth=1'
alias gcom='git commit -m'
alias gdiff='git diff'
alias glog='git log'
alias gst='git status -s'
alias gpull='git pull origin'
alias gpush='git push origin'

# -- Plugins --

# GIT PROMPT

GIT_PS1_SHOWDIRTYSTATE=true

source /usr/lib/git-core/git-sh-prompt
source /usr/share/bash-completion/completions/git
__git_complete g __git_main
__git_complete gcl _git_clone

# Bash Completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#FZF
#. /usr/share/doc/fzf/completion.bash
. /usr/share/doc/fzf/examples/key-bindings.bash
. /usr/share/bash-completion/completions/fzf
